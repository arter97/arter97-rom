#!/sbin/sh
cd /tmp/arter97/patch
find . -type f -exec chmod 644 {} \;
find . -type d -exec chmod 755 {} \;
chown root:shell /tmp/arter97/patch/bin/*
chmod 755 /tmp/arter97/patch/bin/*
cp -rp /tmp/arter97/patch/priv-app/* /system/priv-app/
cp -rp /tmp/arter97/patch/lib/* /system/lib/
cp -rp /tmp/arter97/patch/bin/* /system/bin/
cp -rp /tmp/arter97/patch/framework/* /system/framework/
cp -rp /tmp/arter97/patch/etc/* /system/etc/
sed -i	-e 's/ro.product.model=GT-I9300/ro.product.model=SHV-E210S/g' \
	-e 's/ro.product.locale.language=en/ro.product.locale.language=ko/g' \
	-e 's/ro.product.locale.region=US/ro.product.locale.region=KR/g' \
	-e 's/-UNOFFICIAL-i9300/-UNOFFICIAL-e210s/g' /system/build.prop
sed -i -e 's/dalvik.vm.dexopt-data-only=1/dalvik.vm.dexopt-data-only=1\ntelephony.lteOnGsmDevice=1\nro.telephony.default_network=9\nro.ril.def.preferred.network=9/g' /system/build.prop
sed -i -e 's/ro.com.google.clientidbase=android-google/ro.com.google.clientidbase=android-motorola\nro.com.google.clientidbase.ms=android-skt-kr\nro.com.google.clientidbase.am=android-skt-kr\nro.com.google.clientidbase.gmm=android-skt-kr\nro.com.google.clientidbase.yt=android-skt-kr/g' /system/build.prop
